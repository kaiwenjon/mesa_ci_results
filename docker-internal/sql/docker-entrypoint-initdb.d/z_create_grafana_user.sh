#!/bin/bash

jenkins_pw=$(cat /run/secrets/mysql-pw-jenkins)

# Create mesaci-web user with RO permissions
user=grafana
pass=$(cat /run/secrets/mysql-pw-$user)
echo creating $user...
mysql -u jenkins -p"$jenkins_pw" <<SQL
CREATE USER '$user'@'%' IDENTIFIED BY '$pass';
GRANT SELECT ON *.* TO '$user'@'%';
FLUSH PRIVILEGES;
SQL
