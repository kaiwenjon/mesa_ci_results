#!/bin/bash

root_pw=$(cat /run/secrets/root)

# Create mesaci-web user with RO permissions
user=mesaci-web
pass=$(cat /run/secrets/$user)
echo creating $user...
mysql -u root -p"$root_pw" <<SQL
CREATE USER '$user'@'' IDENTIFIED BY '$pass';
GRANT SELECT ON *.* TO '$user'@'%';
FLUSH PRIVILEGES;
SQL

# Create mesaci-importer user with RW permissions
user=mesaci-importer
pass=$(cat /run/secrets/$user)
echo creating $user...
mysql -u root -p"$root_pw" <<SQL
CREATE USER '$user'@'' IDENTIFIED BY '$pass';
GRANT ALL PRIVILEGES ON *.* TO '$user'@'%';
FLUSH PRIVILEGES;
SQL

# Create mesaci-db_cleaner user with RW permissions
user=mesaci-db_cleaner
pass=$(cat /run/secrets/$user)
echo creating $user...
mysql -u root -p"$root_pw" <<SQL
CREATE USER '$user'@'' IDENTIFIED BY '$pass';
GRANT ALL PRIVILEGES ON *.* TO '$user'@'%';
FLUSH PRIVILEGES;
SQL
